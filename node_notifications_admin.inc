<?php

/**
 * node_notifications_settings
 */
function node_notifications_settings() {
  $form['notifications_settings'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['notifications'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notification settings'),
    '#group' => 'notifications_settings'
  );
  $form['notifications']['node_notifications_enable_popups'] = array(
    '#type' => 'radios',
    '#title' => t('Enable popups'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => variable_get('node_notifications_enable_popups', '1'),
    '#required' => TRUE,
  );
  $form['notifications']['node_notifications_enable_sidebar'] = array(
    '#type' => 'radios',
    '#title' => t('Enable sidebar'),
    '#options' => array(
      0 => t('No'),
      1 => t('Yes'),
    ),
    '#default_value' => variable_get('node_notifications_enable_sidebar', '1'),
    '#required' => TRUE,
  );
  $form['notifications']['node_notifications_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Message duration'),
    '#description' => t('How long message popup should be shown. 1000 = 1sec.'),
    '#field_suffix' => t('milliseconds'),
    '#default_value' => variable_get('node_notifications_duration', '20000'),
    '#required' => TRUE,
  );
  $form['notifications']['node_notifications_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit of sidebar messages'),
    '#description' => t('How many messages should be shouwn in the sidebar? Use "0" for no limit.'),
    '#default_value' => variable_get('node_notifications_limit', '10'),
    '#required' => TRUE,
  );
  $form['notifications']['node_notifications_period'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar message period'),
    '#description' => t('How old should be messages in the sidebar? Use "0" for no limit.'),
    '#field_suffix' => t('minutes'),
    '#default_value' => variable_get('node_notifications_period', '5'),
    '#required' => TRUE,
  );
  $form['notifications']['node_notifications_server_timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Server timeout'),
    '#description' => t('Timeout of long poll request.'),
    '#field_suffix' => t('seconds'),
    '#default_value' => variable_get('node_notifications_server_timeout', '30'),
    '#required' => TRUE,
  );
  $form['#validate'][] = 'node_notifications_settings_validation';
  
  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content settings'),
    '#group' => 'notifications_settings'
  );

  $form['content']['node_notifications_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Content types'),
    '#description' => t('Choose content type for notifications.'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('node_notifications_content_type'),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

function node_notifications_settings_validation($form, &$form_state) {
  $error = FALSE;
  foreach ($form_state['values'] as $key => $value) {
    if (strpos($key, 'node_notifications') !== false && $key != 'node_notifications_content_type') {
      if (!is_numeric($value)) {
        form_set_error($key, t('You can set only numeric values'));
        $error = TRUE;
      }
    }
  }
}

