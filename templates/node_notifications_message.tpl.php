<div id="node-notifications-message<?php print $class_suffix . '-' . $nid; ?>" 
     class="node-notifications-message<?php print $class_suffix; ?>  messages warning">

  <!-- message -->
  <?php if (!empty($message)) : ?>
    <div>
      <?php print $message; ?>
    </div>
  <?php endif; ?>
  
  <!-- created -->
  <?php if (!empty($created)) : ?>
    <div>
      <label><?php print t('Date'); ?>: </label>
      <?php print $created; ?>
    </div>
  <?php endif; ?>

  <!-- more-->
  <div class="node-notifications-more">
    <a href="<?php print $url; ?>">
      <?php print $more_text; ?> >>
    </a>
  </div>

  <!-- close -->
  <?php if ($close) : ?>
    <i class="node-notifications-close">x</i>
  <?php endif; ?>
    
</div>
