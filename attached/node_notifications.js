(function ($) {

  Drupal.behaviors.node_notifications = {
    attach: function (context, settings) {   
      // If popups enabled
      if(Drupal.settings.node_notifications_enable_popups == 1){
        // Start the inital request
        waitForMsg();
      }
      
      /**
       * Show message
       */
      function addmsg(data){
        if(data.action == 'showPopup'){
          // Message id
          var id = 'node-notifications-message-' + data.id;
          // Show message
          $('#node-notifications-messages').append(data.message);
          if(!$('#node-notifications-message-panel-' + data.id).length)
            $('#node-notifications-panel-inner').append(data.message_panel);
          $('#' + id).fadeIn(500);
          // Hide after delay
          $('#' + id).delay(data.delay).animate({
            opacity: 0
          }, {
            duration: 300,
            complete: function() {
              $(this).remove();
            }
          });          
        }
      }
      
      /**
       *  Requests the url. When it complete (or errors)
       */
      function waitForMsg(){
        $.ajax({
          type: 'POST',
          url: '/admin/node_notifications/ajax',
          data: { 
            action: 'showPopup'
          },
          dataType: 'json',
          async: true,
          cache: false,
          timeout:50000,

          success: function(data){
            if(data != undefined && data[0] != undefined)
              $.each(data, function(i, val) {
                // Add response message
                addmsg(val);
                deleteMsg(val.id)
              });
             
            setTimeout(
              waitForMsg, // Request next message
              1000 // ..after 1 second
              );
          },
          error: function(){
            //addmsg('error', textStatus + ' (' + errorThrown + ')');
            setTimeout(
              waitForMsg, // Try again after.. 
              15000); // milliseconds (15seconds)
          }
        });
      }
      
      // Close message
      $('.node-notifications-close').live('click', function(){
        $(this).closest('.node-notifications-message').remove();
      });
      
      // Close message
      $('#node-notifications-panel .node-notifications-panel-toggle').toggle(
        function(){
          $('#node-notifications-panel').animate({
            right: '0'
          });
        },
        function(){
          $('#node-notifications-panel').animate({
            right: '-300px'
          });
        }
        );
      
      /**
       *  Requests the url. When it complete (or errors)
       */
      function deleteMsg(id){
        $.ajax({
          type: 'POST',
          url: '/admin/node_notifications/ajax',
          data: { 
            action: 'updateAsShown',
            id: id
          }
        });
      }
    }
  };

})(jQuery);