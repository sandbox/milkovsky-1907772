<?php

/**
 * NodeNotificationsModel model. Contains CRUD functions.
 */
class NodeNotificationsModel {

  private function _table() {
    return 'node_notifications';
  }

  /**
   * Select all events
   */
  public function select($not_shown = FALSE, $limit = 0, $period = 0) {
    $query = db_select('node_notifications', 't');
    $query->leftJoin('node', 'n', 'n.nid = t.nid');
    if ($not_shown) {
      $query->condition('t.status', '1', '!=');
    }
    if ($period > 0) {
      $query->condition('t.created', $period, '>');
    }
    $query->fields('t', array('id', 'created', 'nid', 'status'));
    $query->fields('n', array('title'));
    if ($limit > 0) {
      $query->range(0, $limit);
    }
    return $query->execute();
  }

  /**
   * Inserts new event in DB
   */
  public function insert($title, $action, $nid, $status) {
    return db_insert($this->_table())
        ->fields(array(
          'nid' => $nid,
          'created' => time(),
          'status' => $status,
        ))
        ->execute();
  }

  /**
   * Update event status
   */
  public function updateSetStatus($id, $status) {
    return db_update($this->_table())
        ->fields(array('status' => $status))
        ->condition('id', $id)
        ->execute();
  }

  /**
   * Deletes event from DB
   */
  public function delete($id) {
    return db_delete($this->_table())
        ->condition('id', $id)
        ->execute();
  }

}

